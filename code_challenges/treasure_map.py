""" Treasure map """

row1 = ["O", "O", "O"]
row2 = ["O", "O", "O"]
row3 = ["O", "O", "O"]
mymap = [row1, row2, row3]
print(f"{row1}\n{row2}\n{row3}")
position = input("Where do you want to put the treasure? ")

# First digit is column, second is row

mymap[int(position[1]) - 1][int(position[0]) - 1] = "X"

# Does mymap hold pointers to rowX? I changed map but am printing rowX

print(f"{row1}\n{row2}\n{row3}")
