""" Rock Paper Scissors """

import random

ROCK = """
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
"""

PAPER = """
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
"""

SCISSORS = """
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
"""

QUEST = "What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors.\n"
hands = [ROCK, PAPER, SCISSORS]
choice = int(input(QUEST))

if choice < 0 or choice > 2:
    print("You typed an invalid number, you lose!")
else:
    print(hands[choice])

    computer = random.randint(0, 2)

    print("Computer choose:\n")

    print(hands[computer])

    if choice == computer:
        print("It's a draw")
    elif (
        (choice == 0 and computer == 2)
        or (choice == 1 and computer == 0)
        or (choice == 2 and computer == 1)
    ):
        print("You win!")
    else:
        print("You lose")
